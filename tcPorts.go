package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"strings"
	"time"
)

var verbose bool = true

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("No arguments given"))
	}

	var proto []string
	var hosts []string
	var hosts_expanded []string
	var ports []string
	var ports_expanded []string
	var timeout time.Duration = 50 * time.Millisecond

	args := os.Args[1:]
	for arg := 0; arg < len(args); arg++ {
		if args[arg] == "--help" {
			fmt.Print("--help\tPrint this help message.\n")
			fmt.Print("--ports=\tComma seperated list of ports to scan. Supports ranges, i.e. x-y\n")
			fmt.Print("--hosts=\tComma seperated list of hosts to scan. Support CIDR ranges and hypenated ranges in\n")
			fmt.Print("\t\tthe last octet, i.e. v.w.x.y-z or a.b.c.d/e\n")
			//fmt.Print("--proto=\tProtocol to use, either: tcp, upd, both     Default=tcp\n")
			fmt.Print("--timeout=\tSet timeout value for each port in milliseconds     Default=50\n")
			fmt.Print("--quiet\t\tTurn off verbose output\n\n")
		}
		regex := regexp.MustCompile(`--ports=(\S+)`)
		if regex.MatchString(args[arg]) {
			ports_list := regex.FindStringSubmatch(args[arg])[1]
			regex = regexp.MustCompile(`(\d+,\S+)`)
			if regex.MatchString(ports_list) {
				ports = strings.Split(regex.FindStringSubmatch(ports_list)[1], ",")
			} else {
				ports = append(ports, ports_list)
			}
		}
		regex = regexp.MustCompile(`--hosts=(\S+)`)
		if regex.MatchString((args[arg])) {
			hosts_list := regex.FindStringSubmatch(args[arg])[1]
			regex = regexp.MustCompile(`\S+,\S+`)
			if regex.MatchString(hosts_list) {
				hosts = strings.Split(regex.FindStringSubmatch(hosts_list)[1], ",")
			} else {
				hosts = append(hosts, hosts_list)
			}
		}
		/*
			regex = regexp.MustCompile(`--proto=(\w+)`)
			if regex.MatchString(args[arg]) {
				switch regex.FindStringSubmatch(args[arg])[1] {
				case "tcp":
					proto = append(proto, "tcp")
				case "udp":
					proto = append(proto, "udp")
				case "both":
					proto = append(proto, "tcp")
					proto = append(proto, "udp")
				}
			}
		*/

		regex = regexp.MustCompile(`--timeout=(\d+)`)
		if regex.MatchString(args[arg]) {
			timeout = time.Duration(regex.FindStringSubmatchIndex(args[arg])[1]) * time.Millisecond
		}
		if args[arg] == "--quiet" {
			verbose = false
		}
	}

	if proto == nil {
		proto = append(proto, "tcp")
	}

	if hosts == nil {
		er(fmt.Errorf("Must provide at least one host"))
	}
	if ports == nil {
		er(fmt.Errorf("Must provide at least one port"))
	}

	for i := 0; i < len(hosts); i++ {
		regex := regexp.MustCompile(`(\d+\.\d+\.\d+\.)(\d+)-(\d+)`)
		regex2 := regexp.MustCompile(`(\d+\.\d+\.\d+\.\d+\/\d+)`)
		if regex.MatchString(hosts[i]) {
			base_ip := regex.FindStringSubmatch(hosts[i])[1]
			start_ip, _ := strconv.Atoi(regex.FindStringSubmatch(hosts[i])[2])
			stop_ip, _ := strconv.Atoi(regex.FindStringSubmatch(hosts[i])[3])

			for j := start_ip; j <= stop_ip; j++ {
				hosts_expanded = append(hosts_expanded, fmt.Sprintf("%s%d", base_ip, j))
			}
		} else if regex2.MatchString(hosts[i]) {
			cidr_hosts := cidr_range(hosts[i])
			for k := 0; k < len(cidr_hosts); k++ {
				hosts_expanded = append(hosts_expanded, cidr_hosts[k])
			}
		} else {
			hosts_expanded = append(hosts_expanded, hosts[i])
		}
	}

	hosts = hosts_expanded

	for i := 0; i < len(ports); i++ {
		regex := regexp.MustCompile(`(\d+)-(\d+)`)
		if regex.MatchString(ports[i]) {
			start_port, _ := strconv.Atoi(regex.FindStringSubmatch(ports[i])[1])
			stop_port, _ := strconv.Atoi(regex.FindStringSubmatch(ports[i])[2])

			for j := start_port; j <= stop_port; j++ {
				ports_expanded = append(ports_expanded, fmt.Sprintf("%d", j))
			}
		} else {
			ports_expanded = append(ports_expanded, ports[i])
		}
	}

	ports = ports_expanded

	for i := 0; i < len(hosts); i++ {
		scan_host(hosts[i], ports, proto, timeout)
	}
	os.Exit(0)
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error with caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func cidr_range(cidr string) []string {
	_, network, err := net.ParseCIDR(cidr)
	if err != nil {
		log.Fatalf("ERROR: %v", err)
		os.Exit(1)
	}

	var ips []string
	var octet int
	var net_id [4]byte
	var net_mask [4]byte
	var broadcast [4]byte

	for i := 0; i < 4; i++ {
		octet, _ = strconv.Atoi(fmt.Sprintf("%d", network.IP[i]))
		net_id[i] = byte(octet)
		octet, _ = strconv.Atoi(fmt.Sprintf("%d", network.Mask[i]))
		net_mask[i] = byte(octet)
		bits := net_mask[i] ^ 0xFF
		broadcast[i] = net_id[i] | bits
	}

	for oct1 := int(net_id[0]); oct1 <= int(broadcast[0]); oct1++ {
		for oct2 := int(net_id[1]); oct2 <= int(broadcast[1]); oct2++ {
			for oct3 := int(net_id[2]); oct3 <= int(broadcast[2]); oct3++ {
				for oct4 := int(net_id[3]); oct4 <= int(broadcast[3]); oct4++ {
					ips = append(ips, fmt.Sprintf("%d.%d.%d.%d", oct1, oct2, oct3, oct4))
				}
			}
		}
	}

	return ips[1 : len(ips)-1]
}

func scan_host(ip string, ports []string, proto []string, timeout time.Duration) {
	for port := 0; port < len(ports); port++ {
		for n := 0; n < len(proto); n++ {
			debug(fmt.Sprintf("%s:%s  ", ip, ports[port]))

			conn, err := net.DialTimeout(proto[n], fmt.Sprintf("%s:%s", ip, ports[port]), timeout)
			if err != nil {
				fmt.Print("\r                                                                                           \r")
				continue
			}

			defer conn.Close()

			connbuf := bufio.NewReader(conn)
			conn.SetReadDeadline(time.Now().Add(1 * time.Second))
			output, err := connbuf.ReadString('\n')
			if err != nil {
				if len(fmt.Sprintf("%s:%s", ip, ports[port])) < 14 {
					debug(fmt.Sprint("\t\tOPEN\n"))
				} else {
					debug(fmt.Sprint("\tOPEN\n"))
				}
			} else {
				if len(fmt.Sprintf("%s:%s", ip, ports[port])) < 14 {
					debug(fmt.Sprintf("\t\t%s\n", strings.TrimSuffix(output, "\n")))
				} else {
					debug(fmt.Sprintf("\t%s\n", strings.TrimSuffix(output, "\n")))
				}
			}

		}
	}

}

func debug(text string) {
	if verbose {
		fmt.Print(text)
	}
}
